#!/usr/bin/perl

use strict;

use FileHandle;
use HTML::TreeBuilder;
use Data::Dumper;
$Data::Dumper::Indent= 1;

binmode STDOUT, ':utf8';

my $t1= <<"EOX";
<html>
  <head>
    <title>bla</title>
  </head>
  <body>
    <p ID="par1">Pargraph1</p>
    <p ID="par2">Pargraph2 Universit\x{e4}t Wien</p>
    <p ID="par3">Pargraph3</p>
    <p ID="par4">Pargraph4</p>
  </body>
</html>
EOX

my $htb1= HTML::TreeBuilder->new_from_content($t1);

print "htb1: ", Dumper ($htb1);
my @x= $htb1->look_down (_tag => 'p');
print "x: ", Dumper (\@x);
